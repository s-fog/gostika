<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Review;

/**
* ReviewSearch represents the model behind the search form about `common\models\Review`.
*/
class ReviewSearch extends Review
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'date', 'mark', 'created_at', 'updated_at'], 'integer'],
            [['name', 'en_name', 'header', 'en_header', 'text', 'en_text', 'active'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Review::find();

$dataProvider = new ActiveDataProvider([
'query' => $query,
]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'mark' => $this->mark,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'en_name', $this->en_name])
            ->andFilterWhere(['like', 'header', $this->header])
            ->andFilterWhere(['like', 'en_header', $this->en_header])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['active' => $this->active])
            ->andFilterWhere(['like', 'en_text', $this->en_text]);

return $dataProvider;
}
}