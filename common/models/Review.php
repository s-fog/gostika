<?php

namespace common\models;

use frontend\models\CustomDate;
use Yii;
use \common\models\base\Review as BaseReview;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "review".
 */
class Review extends BaseReview
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'en_name', 'mark', 'header', 'en_header', 'text', 'en_text', 'phone'], 'required'],
            [['mark', 'active'], 'integer'],
            [['date'], 'safe'],
            [['name', 'en_name', 'header', 'en_header', 'text', 'en_text', 'phone'], 'string', 'max' => 255]
        ];
    }

    public function getLang_name() {
        if (Yii::$app->language == 'en') {
            return $this->en_name;
        } else {
            return $this->name;
        }
    }

    public function getLang_header() {
        if (Yii::$app->language == 'en') {
            return $this->en_header;
        } else {
            return $this->header;
        }
    }

    public function getLang_text() {
        if (Yii::$app->language == 'en') {
            return $this->en_text;
        } else {
            return $this->text;
        }
    }

    public function getLang_date() {
        if (Yii::$app->language == 'en') {
            return date("d m Y", $this->date);
        } else {
            return date("d", $this->date).' '.CustomDate::month($this->date).' '.date("Y", $this->date);
        }
    }
}
