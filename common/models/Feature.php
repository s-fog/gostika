<?php

namespace common\models;

use Yii;
use \common\models\base\Feature as BaseFeature;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "feature".
 */
class Feature extends BaseFeature
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                # custom validation rules
            ]
        );
    }

    public function getLang_name() {
        if (Yii::$app->language == 'en') {
            return $this->en_name;
        } else {
            return $this->name;
        }
    }
}
