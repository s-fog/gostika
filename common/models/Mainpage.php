<?php

namespace common\models;

use Yii;
use \common\models\base\Mainpage as BaseMainpage;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mainpage".
 */
class Mainpage extends BaseMainpage
{

public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                # custom behaviors
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'en_name', 'phone', 'email', 'address', 'en_address', 'advantages', 'en_advantages', 'rules', 'en_rules', 'about', 'en_about'], 'required'],
            [['advantages', 'en_advantages', 'rules', 'en_rules', 'about', 'en_about'], 'string'],
            [['name', 'en_name', 'seo_description', 'advantages', 'en_advantages', 'rules', 'en_rules', 'about', 'en_about'], 'string'],
            [['seo_title', 'en_seo_title'], 'string', 'max' => 80],
            [['seo_description', 'en_seo_description'], 'string', 'max' => 150],
        ];
    }
    public function getLang_seo_h1() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_h1;
        } else {
            return $this->seo_h1;
        }
    }

    public function getLang_seo_title() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_title;
        } else {
            return $this->seo_title;
        }
    }

    public function getLang_seo_description() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_description;
        } else {
            return $this->seo_description;
        }
    }

    public function getLang_seo_keywords() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_keywords;
        } else {
            return $this->seo_keywords;
        }
    }

    public function getLang_name() {
        if (Yii::$app->language == 'en') {
            return $this->en_name;
        } else {
            return $this->name;
        }
    }

    public function getLang_advantages() {
        if (Yii::$app->language == 'en') {
            return $this->en_advantages;
        } else {
            return $this->advantages;
        }
    }

    public function getLang_rules() {
        if (Yii::$app->language == 'en') {
            return $this->en_rules;
        } else {
            return $this->rules;
        }
    }

    public function getLang_about() {
        if (Yii::$app->language == 'en') {
            return $this->en_about;
        } else {
            return $this->about;
        }
    }

    public function getLang_address() {
        if (Yii::$app->language == 'en') {
            return $this->en_address;
        } else {
            return $this->address;
        }
    }
}
