<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use \common\models\base\Room as BaseRoom;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "room".
 */
class Room extends BaseRoom
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true,
                    'ensureUnique' => true,
                ],
                'sort' => [
                    'class' => SortableGridBehavior::className(),
                    'sortableAttribute' => 'sortOrder'
                ],
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'en_name', 'price', 'descr', 'en_descr', 'category'], 'required'],
            ['image1', 'required', 'on' => 'create'],
            [['descr', 'en_descr'], 'string'],
            [['price', 'hot', 'sale', 'coffe', 'light', 'bed', 'dinner', 'category', 'show_on_main', 'sortOrder'], 'integer'],
            [['name', 'en_name', 'alias', 'seo_keywords', 'en_seo_keywords', 'seo_h1', 'en_seo_h1'], 'string', 'max' => 255],
            [['image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7', 'image8', 'image9', 'image10', 'image11', 'image12', 'image13', 'image14', ], 'image', 'skipOnEmpty' => true, 'minWidth' => 975, 'minHeight' => 560, 'extensions' => 'png, jpg, jpeg'],
            [['seo_title', 'en_seo_title'], 'string', 'max' => 80],
            [['seo_description', 'en_seo_description'], 'string', 'max' => 150],
        ];
    }

    public function getFeatures() {
        return $this->hasMany(Feature::className(), ['room_id' => 'id']);
    }

    public function getFeatures_preview() {
        return Feature::find()
            ->where([
                'room_id' => $this->id,
                'on_preview' => 1,
            ])
            ->all();
    }

    public function getFeatures_room() {
        return Feature::find()
            ->where([
                'room_id' => $this->id,
                'type' => 0,
            ])
            ->all();
    }

    public function getFeatures_bath() {
        return Feature::find()
            ->where([
                'room_id' => $this->id,
                'type' => 1,
            ])
            ->all();
    }

    public function getFeatures_additional() {
        return Feature::find()
            ->where([
                'room_id' => $this->id,
                'type' => 2,
            ])
            ->all();
    }

    public function getUrl() {
        return Url::to([
            'site/index',
            'alias' => Textpage::findOne(1)->alias,
            'alias2' => Textpage::findOne($this->category)->alias,
            'alias3' => $this->alias,
        ]);
    }

    public function getLang_descr() {
        if (Yii::$app->language == 'en') {
            return $this->en_descr;
        } else {
            return $this->descr;
        }
    }

    public function getLang_name() {
        if (Yii::$app->language == 'en') {
            return $this->en_name;
        } else {
            return $this->name;
        }
    }

    public function getLang_seo_h1() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_h1;
        } else {
            return $this->seo_h1;
        }
    }

    public function getLang_seo_title() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_title;
        } else {
            return $this->seo_title;
        }
    }

    public function getLang_seo_description() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_description;
        } else {
            return $this->seo_description;
        }
    }

    public function getLang_seo_keywords() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_keywords;
        } else {
            return $this->seo_keywords;
        }
    }
}
