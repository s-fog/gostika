<?php

namespace common\models;

use himiklab\sortablegrid\SortableGridBehavior;
use Yii;
use \common\models\base\Textpage as BaseTextpage;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "textpage".
 */
class Textpage extends BaseTextpage
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'name',
                    'slugAttribute' => 'alias',
                    'immutable' => true,
                    'ensureUnique' => true,
                ],
                'sort' => [
                    'class' => SortableGridBehavior::className(),
                    'sortableAttribute' => 'sortOrder'
                ],
            ]
        );
    }

    public function rules()
    {
        return [
            [['name', 'en_name'], 'required'],
            [['html', 'en_html', 'anons', 'en_anons', 'table', 'en_table'], 'string'],
            [['parent_id', 'sortOrder'], 'integer'],
            [['image'], 'safe'],
            [['name', 'en_name', 'alias', 'seo_keywords', 'en_seo_keywords', 'seo_h1', 'en_seo_h1'], 'string', 'max' => 255],
            [['seo_title', 'en_seo_title'], 'string', 'max' => 80],
            [['seo_description', 'en_seo_description'], 'string', 'max' => 150],
        ];
    }

    public static function getTextpages($id = NULL) {
        $textpages = Textpage::find()->where([])->all();
        $result = [];
        $result[0] = 'Нет родителя';

        if ($id == NULL) {
            foreach($textpages as $p) {
                $result[$p['id']] = $p['name'];
            }
        } else {
            foreach($textpages as $p) {
                if ($id != $p['id'])
                    $result[$p['id']] = $p['name'];
            }
        }

        return $result;
    }

    public function getLang_name() {
        if (Yii::$app->language == 'en') {
            return $this->en_name;
        } else {
            return $this->name;
        }
    }

    public function getUrl() {
        if ($this->parent_id == 1) {
            return Url::to(['site/index', 'alias' => Textpage::findOne(1)->alias, 'alias2' => $this->alias]);
        } else {
            return Url::to(['site/index', 'alias' => $this->alias]);
        }
    }

    public function liLink() {
        $url = $this->url;

        if ($_SERVER['REQUEST_URI'] == $url) {
            return '<li class="active"><span>'.$this->lang_name.'</span></li>';
        } else {
            return '<li><a href="'.$url.'">'.$this->lang_name.'</a></li>';
        }
    }
    public function getLang_seo_h1() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_h1;
        } else {
            return $this->seo_h1;
        }
    }

    public function getLang_seo_title() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_title;
        } else {
            return $this->seo_title;
        }
    }

    public function getLang_seo_description() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_description;
        } else {
            return $this->seo_description;
        }
    }

    public function getLang_seo_keywords() {
        if (Yii::$app->language == 'en') {
            return $this->en_seo_keywords;
        } else {
            return $this->seo_keywords;
        }
    }

    public function getLang_html() {
        if (Yii::$app->language == 'en') {
            return $this->en_html;
        } else {
            return $this->html;
        }
    }

    public function getLang_anons() {
        if (Yii::$app->language == 'en') {
            return $this->en_anons;
        } else {
            return $this->anons;
        }
    }

    public function getLang_table() {
        if (Yii::$app->language == 'en') {
            return $this->en_table;
        } else {
            return $this->table;
        }
    }
}
