<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Room;

/**
* RoomSearch represents the model behind the search form about `common\models\Room`.
*/
class RoomSearch extends Room
{
/**
* @inheritdoc
*/
public function rules()
{
return [
[['id', 'created_at', 'updated_at', 'price', 'hot', 'sale', 'coffe', 'light', 'bed', 'dinner'], 'integer'],
            [['name', 'en_name', 'alias', 'seo_title', 'en_seo_title', 'seo_keywords', 'en_seo_keywords', 'seo_h1', 'en_seo_h1', 'seo_description', 'en_seo_description', 'descr', 'en_descr', 'image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7', 'image8', 'image9', 'image10', 'image11', 'image12', 'image13', 'image14'], 'safe'],
];
}

/**
* @inheritdoc
*/
public function scenarios()
{
// bypass scenarios() implementation in the parent class
return Model::scenarios();
}

/**
* Creates data provider instance with search query applied
*
* @param array $params
*
* @return ActiveDataProvider
*/
public function search($params)
{
$query = Room::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 100 ],
        ]);

$this->load($params);

if (!$this->validate()) {
// uncomment the following line if you do not want to any records when validation fails
// $query->where('0=1');
return $dataProvider;
}

$query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'price' => $this->price,
            'hot' => $this->hot,
            'sale' => $this->sale,
            'coffe' => $this->coffe,
            'light' => $this->light,
            'bed' => $this->bed,
            'dinner' => $this->dinner,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'en_name', $this->en_name])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'seo_title', $this->seo_title])
            ->andFilterWhere(['like', 'en_seo_title', $this->en_seo_title])
            ->andFilterWhere(['like', 'seo_keywords', $this->seo_keywords])
            ->andFilterWhere(['like', 'en_seo_keywords', $this->en_seo_keywords])
            ->andFilterWhere(['like', 'seo_h1', $this->seo_h1])
            ->andFilterWhere(['like', 'en_seo_h1', $this->en_seo_h1])
            ->andFilterWhere(['like', 'seo_description', $this->seo_description])
            ->andFilterWhere(['like', 'en_seo_description', $this->en_seo_description])
            ->andFilterWhere(['like', 'descr', $this->descr])
            ->andFilterWhere(['like', 'en_descr', $this->en_descr])
            ->andFilterWhere(['like', 'image1', $this->image1])
            ->andFilterWhere(['like', 'image2', $this->image2])
            ->andFilterWhere(['like', 'image3', $this->image3])
            ->andFilterWhere(['like', 'image4', $this->image4])
            ->andFilterWhere(['like', 'image5', $this->image5])
            ->andFilterWhere(['like', 'image6', $this->image6])
            ->andFilterWhere(['like', 'image7', $this->image7])
            ->andFilterWhere(['like', 'image8', $this->image8])
            ->andFilterWhere(['like', 'image9', $this->image9])
            ->andFilterWhere(['like', 'image10', $this->image10])
            ->andFilterWhere(['like', 'image11', $this->image11])
            ->andFilterWhere(['like', 'image12', $this->image12])
            ->andFilterWhere(['like', 'image13', $this->image13])
            ->andFilterWhere(['like', 'image14', $this->image14]);

return $dataProvider;
}
}