<?php
use common\models\Textpage;

$category = Textpage::findOne($model->category);
?>
<div class="room-content">
    <div class="slider-container">
        <div class="slider-big">
            <div class="room-arrows slide-arrows">
                <div class="arrow arrow-prev"><i class="icon icon-slider_prev"></i></div>
                <div class="arrow arrow-next"><i class="icon icon-slider_next"></i></div>
            </div>
            <div class="room-slider room-single-slider">
                <?php if (!empty($model->image1)) {
                    $filename = explode('.', basename($model->image1));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image2)) {
                    $filename = explode('.', basename($model->image2));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image3)) {
                    $filename = explode('.', basename($model->image3));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image4)) {
                    $filename = explode('.', basename($model->image4));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image5)) {
                    $filename = explode('.', basename($model->image5));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image6)) {
                    $filename = explode('.', basename($model->image6));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image7)) {
                    $filename = explode('.', basename($model->image7));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image8)) {
                    $filename = explode('.', basename($model->image8));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image9)) {
                    $filename = explode('.', basename($model->image9));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image10)) {
                    $filename = explode('.', basename($model->image10));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image11)) {
                    $filename = explode('.', basename($model->image11));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image12)) {
                    $filename = explode('.', basename($model->image12));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image13)) {
                    $filename = explode('.', basename($model->image13));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
                <?php if (!empty($model->image14)) {
                    $filename = explode('.', basename($model->image14));
                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                    ?>
                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="room-type">
        <?=($model->hot == 1) ? '<div class="status">Hot</div>' : ''?>
        <?=($model->sale == 1) ? '<div class="status">Sale</div>' : ''?>
        <h3><a href="<?=$model->url?>"><?=$category->lang_name?></a></h3>
        <div class="price"><?=Yii::t('translate', 'from')?> <strong><?=$model->price?></strong> <i class="rub">&#8381;</i>/<?=Yii::t('translate', 'night')?></div>
        <a href="#order-popup" class="btn popup-open" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'book')?></span></a>
        <a href="<?=$model->url?>" class="simple-btn"><span><?=Yii::t('translate', 'more')?></span></a>
    </div>
    <?php if (!empty($model->features_preview)) { ?>
        <div class="room-description">
            <div class="facilities-block">
                <h6><?=Yii::t('translate', 'Available amenities')?>:</h6>
                <ul class="facilities-list">
                    <?php foreach($model->features_preview as $index => $reviewItem) {
                        $d = 'right';
                        if ($index % 2 == 0) {
                            $d = 'left';
                        }
                        ?>
                        <li>
                            <div class="img-block"> <i class="fa <?=$reviewItem->icon?>"></i></div>
                            <div class="desc <?=$d?>"><?=$reviewItem->lang_name?></div>
                        </li>
                    <?php } ?>
                </ul>
                <a href="<?=$model->url?>" class="arrow-link"><span><?=Yii::t('translate', 'all')?></span> <i class="icon icon-long_arrow"></i></a>
            </div>
        </div>
    <?php } ?>
</div>