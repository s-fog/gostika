<?php

use common\models\Textpage;
use yii\helpers\Url;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

$superParent = Textpage::findOne(1);
$category = Textpage::findOne($model->category);

?>

<!--main-content-->
<div class="main-content">
    <!-- =================== ЗАГОЛОВОК ======================= -->
    <section class="title-section">
        <div class="container">
            <div class="title-content">
                <ul class="breadcrumbs">
                    <li><a href="<?=Url::to(['site/index', 'alias' => $superParent->alias])?>"><?=$superParent->lang_name?></a></li>
                    <li><a href="<?=Url::to(['site/index', 'alias' => $superParent->alias, 'alias2' => $category->alias])?>"><?=$category->lang_name?></a></li>
                </ul>
                <h1><?=(!empty($model->lang_seo_h1)) ? $model->lang_seo_h1 : $model->lang_name?></h1>
            </div>
        </div>
    </section>
    <!-- =================== НОМЕРА ======================= -->
    <section class="rooms-section">
        <div class="room-container">
            <div class="container">
                <div class="room-content single-room">
                    <div class="room-type">
                        <?=($model->hot == 1) ? '<div class="status">Hot</div>' : ''?>
                        <?=($model->sale == 1) ? '<div class="status">Sale</div>' : ''?>
                        <h3><?=$category->lang_name?></h3>
                        <div class="price"><?=Yii::t('translate', 'from')?> <strong><?=$model->price?></strong> <i class="rub">&#8381;</i>/<?=Yii::t('translate', 'night')?></div>
                        <a href="#order-popup" class="btn popup-open" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'book')?></span></a>
                        <p><?=$model->lang_descr?></p>
                        <p><strong><?=Yii::t('translate', 'checkout time')?> - 12:00</strong></p>
                    </div>
                    <div class="room-description">
                        <div class="facilities-block">
                            <h6><?=Yii::t('translate', 'Available amenities')?>:</h6>
                            <ul class="facilities-list">
                                <?php foreach($model->features_preview as $index => $reviewItem) {
                                    $d = 'right';
                                    if ($index % 2 == 0) {
                                        $d = 'left';
                                    }
                                    ?>
                                    <li>
                                        <div class="img-block"> <i class="fa <?=$reviewItem->icon?>"></i></div>
                                        <div class="desc <?=$d?>"><?=$reviewItem->lang_name?></div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <ul class="room-more-info">
                            <li>
                                <div class="img-block"><i class="icon icon-key"></i></div>
                                <span><?=Yii::t('translate', 'fr1')?></span>
                            </li>
                            <li>
                                <div class="img-block"><i class="icon icon-card"></i></div>
                                <span><?=Yii::t('translate', 'fr2')?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="slider-container">
                        <div class="slider-big">
                            <div class="room-arrows slide-arrows">
                                <div class="arrow arrow-prev"><i class="icon icon-slider_prev"></i></div>
                                <div class="arrow arrow-next"><i class="icon icon-slider_next"></i></div>
                            </div>
                            <div class="room-slider room-nav-slider">
                                <?php if (!empty($model->image1)) {
                                    $filename = explode('.', basename($model->image1));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image2)) {
                                    $filename = explode('.', basename($model->image2));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image3)) {
                                    $filename = explode('.', basename($model->image3));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image4)) {
                                    $filename = explode('.', basename($model->image4));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image5)) {
                                    $filename = explode('.', basename($model->image5));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image6)) {
                                    $filename = explode('.', basename($model->image6));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image7)) {
                                    $filename = explode('.', basename($model->image7));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image8)) {
                                    $filename = explode('.', basename($model->image8));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image9)) {
                                    $filename = explode('.', basename($model->image9));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image10)) {
                                    $filename = explode('.', basename($model->image10));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image11)) {
                                    $filename = explode('.', basename($model->image11));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image12)) {
                                    $filename = explode('.', basename($model->image12));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image13)) {
                                    $filename = explode('.', basename($model->image13));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                                <?php if (!empty($model->image14)) {
                                    $filename = explode('.', basename($model->image14));
                                    $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                    ?>
                                    <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="room-slider-small">
                            <?php if (!empty($model->image1)) {
                                $filename = explode('.', basename($model->image1));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image2)) {
                                $filename = explode('.', basename($model->image2));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image3)) {
                                $filename = explode('.', basename($model->image3));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image4)) {
                                $filename = explode('.', basename($model->image4));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image5)) {
                                $filename = explode('.', basename($model->image5));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image6)) {
                                $filename = explode('.', basename($model->image6));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image7)) {
                                $filename = explode('.', basename($model->image7));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image8)) {
                                $filename = explode('.', basename($model->image8));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image9)) {
                                $filename = explode('.', basename($model->image9));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image10)) {
                                $filename = explode('.', basename($model->image10));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image11)) {
                                $filename = explode('.', basename($model->image11));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image12)) {
                                $filename = explode('.', basename($model->image12));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image13)) {
                                $filename = explode('.', basename($model->image13));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                            <?php if (!empty($model->image14)) {
                                $filename = explode('.', basename($model->image14));
                                $image = "/images/thumbs/$filename[0]-209-120.$filename[1]";
                                ?>
                                <div class="slide"><img src="<?=$image?>" alt="" /></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =================== ИНФОРМАЦИЯ О НОМЕРЕ ======================= -->
    <section class="information-section">
        <div class="container">
            <div class="room-info-container">
                <?php $features_room = $model->features_room;
                if (!empty($features_room)) { ?>
                    <ul class="room-info-list">
                        <li class="title"><div class="img-block"><i class="fa fa-coffee"></i></div><?=Yii::t('translate', 'in room')?>:</li>
                        <?php foreach($features_room as $item) { ?>
                            <li><div class="img-block"><i class="fa <?=$item->icon?>"></i></div><?=$item->lang_name?></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php $features_bath = $model->features_bath;
                if (!empty($features_bath)) { ?>
                    <ul class="room-info-list">
                        <li class="title"><div class="img-block"><i class="fa fa-coffee"></i></div><?=Yii::t('translate', 'in bath')?>:</li>
                        <?php foreach($features_bath as $item) { ?>
                            <li><div class="img-block"><i class="fa <?=$item->icon?>"></i></div><?=$item->lang_name?></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php $features_additional = $model->features_additional;
                if (!empty($features_additional)) { ?>
                    <ul class="room-info-list">
                        <li class="title"><div class="img-block"><i class="fa fa-coffee"></i></div><?=Yii::t('translate', 'additional')?>:</li>
                        <?php foreach($features_additional as $item) { ?>
                            <li><div class="img-block"><i class="fa <?=$item->icon?>"></i></div><?=$item->lang_name?></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </section>
    <!-- =================== фиксированная линия ======================= -->
    <div id="fixed-bottom" class="fixed-bottom-line">
        <div class="container">
            <div class="line-content">
                <h3><?=$category->lang_name?></h3>
                <div class="price"><?=Yii::t('translate', 'from')?> <strong><?=$model->price?></strong> <i class="rub">₽</i>/<?=Yii::t('translate', 'night')?></div>
                <a data-effect="mfp-zoom-in" class="btn popup-open" href="#order-popup"><span><?=Yii::t('translate', 'book')?></span></a>
            </div>
        </div>
    </div>
</div>
<!--//main-content-->