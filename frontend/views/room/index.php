<?php

use common\models\Mainpage;
use common\models\Textpage;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

$mainPage = Mainpage::findOne(1);

?>

<!--main-content-->
<div class="main-content">

    <!-- =============== ИНФОРМАЦИЯ =================== -->
    <section class="order-title-section">
        <?=$this->render('@frontend/views/blocks/bron', ['model' => $model])?>
        <div class="container">
            <ul class="breadcrumbs">
                <?php if ($model->parent_id == 1) {
                    $parent = Textpage::findOne(1);
                    ?>
                    <li><a href="<?=$parent->url?>"><?=$parent->lang_name?></a></li>
                    <li><a href="<?=$model->url?>"><?=$model->lang_name?></a></li>
                <?php } else { ?>
                    <li><a href="<?=$model->url?>"><?=$model->lang_name?></a></li>
                <?php }  ?>
            </ul>
            <h1><?=(!empty($model->lang_seo_h1)) ? $model->lang_seo_h1 : $model->lang_name?></h1>
            <?=$mainPage->lang_advantages?>
        </div>
    </section>

    <!-- =============== НАШИ НОМЕРА =================== -->
    <section class="our-rooms-section">
        <div class="container">
            <h2><?=Yii::t('translate', 'ourRooms')?></h2>
            <div class="our-rooms-outer">
                <div class="our-rooms-content">
                    <?php foreach($rooms as $room) {
                        echo $this->render('@frontend/views/room/_item', ['model' => $room]);
                    } ?>
                </div>
            </div>

            <?=$pagination['html']?>
        </div>
    </section>

    <!-- =============== ПРАВИЛА ЗАЕЗДА =================== -->
    <?=$mainPage->lang_rules?>

    <div class="container">
        <?=$model->lang_html?>
    </div>
</div>
<!--//main-content-->