<div class="review-block active-review">
    <div class="review-author">
        <span class="name"><?=$model->lang_name?></span>
        <span class="date"><?=$model->lang_date?></span>
    </div>
    <div class="review-rating">
        <div class="value"><?=$model->mark?></div>
        <div class="title"><?=$model->lang_header?></div>
    </div>
    <p><?=$model->lang_text?></p>
</div>