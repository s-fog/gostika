<?php

use common\models\Mainpage;
use common\models\Textpage;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

$mainPage = Mainpage::findOne(1);

?>
<div class="main-content">

    <!-- =================== ЗАГОЛОВОК ======================= -->
    <section class="title-section">
        <div class="container">
            <div class="title-content">
                <ul class="breadcrumbs">
                    <li><span><?=$model->lang_name?></span></li>
                </ul>
                <h1><?=(!empty($model->lang_seo_h1)) ? $model->lang_seo_h1 : $model->lang_name?></h1>
            </div>
        </div>
    </section>

    <!-- =================== ОТЗЫВЫ ======================= -->
    <section class="reviews-page-section">
        <div class="container">
            <div class="reviews-page-content" style="position: relative;">
                <?php foreach($reviews as $review) {
                    echo $this->render('@frontend/views/review/_item', ['model' => $review]);
                } ?>
                <div class="total-rating">
                    <div class="value"><?=$averageMark?></div>
                    <p><strong><?=Yii::t('translate', 'mark')?></strong> <?=Yii::t('translate', 'po')?> <?=$reviewsCount?> <?=Yii::t('translate', 'reviews')?></p>
                </div>
            </div>
            <div class="reviews-links">
                <a class="btn popup-open" href="#review-popup" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'takeReview')?></span></a>
                <?=$pagination['html']?>
            </div>
        </div>
    </section>

</div>