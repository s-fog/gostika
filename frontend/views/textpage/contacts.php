<?php

use common\models\Textpage;
use yii\helpers\Url;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

?>

<!--main-content-->
<div class="main-content">
    <section class="title-section">
        <div class="container">
            <div class="title-content">
                <ul class="breadcrumbs">
                    <li><span><?=$model->lang_name?></span></li>
                </ul>
                <h1><?=(!empty($model->lang_seo_h1)) ? $model->lang_seo_h1 : $model->lang_name?></h1>
            </div>
        </div>
    </section>

    <?=$this->render('@frontend/views/blocks/contacts')?>
</div>
<!--//main-content-->