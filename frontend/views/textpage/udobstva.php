<?php

use common\models\Mainpage;
use common\models\Textpage;
use yii\helpers\Url;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

$mainPage = Mainpage::findOne(1);

?>

<!--main-content-->
<div class="main-content">
    <section class="order-title-section">
        <?=$this->render('@frontend/views/blocks/bron')?>
        <div class="container">
            <ul class="breadcrumbs">
                <li><span><?=$model->lang_name?></span></li>
            </ul>
            <h1><?=(!empty($model->lang_seo_h1)) ? $model->lang_seo_h1 : $model->lang_name?></h1>
            <div class="year-price">
                <?=$model->lang_anons?>
            </div>
        </div>
    </section>

    <!-- =============== ТАБЛИЦА ИНФОРМАЦИИ =================== -->
    <section class="info-table-section">
        <div class="container info-container">
            <div class="info-table-outer">
                <?=$model->lang_table?>
            </div>
        </div>
        <div class="container">
            <div class="dop-info">
                <p><?=Yii::t('translate', 'extraCost')?> - 500 <?=Yii::t('translate', 'rubs')?>.</p>
                <p><?=Yii::t('translate', 'checkout time')?> - 12.00</p>
            </div>
        </div>
    </section>

    <!-- =================== ОБ ОТЕЛЕ ======================= -->
    <?=$mainPage->lang_about?>
</div>
<!--//main-content-->