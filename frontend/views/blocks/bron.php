<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="big-img"><img src="<?=(isset($model))? $model->image : '/images/top_section_pic.jpg'?>" alt="" /></div>
        </div>
        <div class="col-md-9">
            <form action="#" method="post" class="order-form">
                <h4 class="visible-xs">Забронировать <small>номер онлайн</small></h4>
                <div class="order-inputs">
                    <label class="input-label">
                        <div class="label-titile"><i class="icon icon-calendar"></i>Заезд</div>
                        <div class="order-date">25</div>
                        <div class="order-month">Апрель</div>
                        <i class="icon icon-select_arrow"></i>
                        <input type="text" class="calendar-input" />
                    </label>
                    <label class="input-label">
                        <div class="label-titile"><i class="icon icon-calendar"></i>Отьезд</div>
                        <div class="order-date">16</div>
                        <div class="order-month">Сентябрь</div>
                        <i class="icon icon-select_arrow"></i>
                        <input type="text" class="calendar-input" />
                    </label>
                    <label class="input-label">
                        <div class="label-titile"><i class="icon icon-man"></i>Человек</div>
                        <div class="order-date">2</div>
                        <div class="order-month">взрослых</div>
                        <i class="icon icon-select_arrow"></i>
                        <input type="text" class="calendar-input"
                    </label>
                </div>
                <div class="order-form-right">
                    <h4>Забронировать <small>номер онлайн</small></h4>
                    <button type="submit" class="btn big-btn"><span>Забронировать</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="line"></div>