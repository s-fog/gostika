<?php

use common\models\Mainpage;

$mainPage = Mainpage::findOne(1);
$phone = $mainPage->phone;

?>
<div class="map-section">
    <div class="maps">
        <div class="maps__item active">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3c68c8f78e9dd2e6bf46df9a2c61d34784be02821cf715db46269150579f9147&amp;width=100%25&amp;height=100%&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
        <div class="maps__item">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Acb3bd8e185f96613d1cb261e2b056ff5c6026189c8ae753b242a53b9565f0d5f&amp;width=100%25&amp;height=100%&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
        <div class="maps__item">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aa20124d325bebd116fd67d9f5562d474453074ecf86970aac89e38cb777d9734&amp;width=100%25&amp;height=100%&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>
    </div>
    <div class="map-content">
        <h3><?=Yii::t('translate', 'address')?></h3>
        <ul>
            <li>
                <div class="img-block"><i class="icon icon-star_marker"></i></div><?=$mainPage->lang_address?>
            </li>
        </ul>
        <h3><?=Yii::t('translate', 'contacts')?></h3>
        <ul>
            <li>
                <div class="img-block"><i class="icon icon-phone"></i></div><a href="tel:<?=str_replace([' ', '(', ')'], ['', '', ''], $phone)?>"><?=$phone?></a>
            </li>
            <li>
                <div class="img-block"><i class="icon icon-letter"></i></div><a href="mailto:<?=$mainPage->email?>"><?=$mainPage->email?></a>
            </li>
        </ul>
        <h3><?=Yii::t('translate', 'arr')?></h3>
        <ul>
            <?php /* ?>
            <li>
                <div class="img-block"><i class="icon icon-car"></i></div><a href=""><?=Yii::t('translate', 'onCar')?></a>
            </li>
            <?php */ ?>
            <li class="switch-map">
                <div class="img-block"><i class="icon icon-metro"></i></div><span><?=Yii::t('translate', 'onMetro')?></span>
            </li>
            <li class="switch-map">
                <div class="img-block"><i class="icon icon-plane"></i></div><span><?=Yii::t('translate', 'fromAirport')?></>
            </li>
        </ul>
    </div>
</div>