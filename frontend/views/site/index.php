<?php

use common\models\Review;
use common\models\Room;
use common\models\Textpage;
use yii\helpers\Url;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

?>

<!--main-content-->
<div class="main-content">

    <!-- =================== СЕКЦИЯ ЗАКАЗА ======================= -->
    <section class="order-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">
                    <form action="#" method="post" class="order-form">
                        <h4>Забронировать <small>номер</small></h4>
                        <div class="order-inputs">
                            <label class="input-label">
                                <div class="label-titile"><i class="icon icon-calendar"></i>Заезд</div>
                                <div class="order-date">25</div>
                                <div class="order-month">Апрель</div>
                                <i class="icon icon-select_arrow"></i>
                                <input type="text" class="calendar-input" />
                            </label>
                            <label class="input-label">
                                <div class="label-titile"><i class="icon icon-calendar"></i>Отьезд</div>
                                <div class="order-date">16</div>
                                <div class="order-month">Сентябрь</div>
                                <i class="icon icon-select_arrow"></i>
                                <input type="text" class="calendar-input" />
                            </label>
                            <label class="input-label">
                                <div class="label-titile"><i class="icon icon-man"></i>Человек</div>
                                <div class="order-date">2</div>
                                <div class="order-month">взрослых</div>
                                <i class="icon icon-select_arrow"></i>
                                <input type="text" class="calendar-input">
                            </label>
                        </div>
                        <button type="submit" class="btn big-btn"><span>Забронировать</span></button>
                        <div class="order-phone">Возникли вопросы?  Тел.  <a href="tel:<?=$model->phone?>"><?=$model->phone?></a></div>
                    </form>
                </div>
                <div class="col-lg-8 col-md-7">
                    <div class="order-img">
                        <div class="img-content">
                            <img src="images/top_section_pic.jpg" alt="" />
                            <div class="img-text">
                                <h1>gostika</h1>
                                <div class="slogan">Уютный мини-отель</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="line"></div>
    </section>

    <!-- =================== ОБ ОТЕЛЕ ======================= -->
    <?=$model->lang_about?>

    <!-- =================== НОМЕРА ======================= -->
    <section class="rooms-section">
        <?php foreach(Room::find()->where(['show_on_main' => 1])->all() as $room) {
            $category = Textpage::findOne($room->category);
            ?>
            <div class="room-container">
                <div class="container">
                    <div class="room-content">
                        <div class="room-type">
                            <?=($room->hot == 1) ? '<div class="status">Hot</div>' : ''?>
                            <?=($room->sale == 1) ? '<div class="status">Sale</div>' : ''?>
                            <h3><?=$category->lang_name?></h3>
                            <div class="price"><?=Yii::t('translate', 'from')?> <strong><?=$room->price?></strong> <i class="rub">&#8381;</i>/<?=Yii::t('translate', 'night')?></div>
                            <p><?=$room->lang_descr?></p>
                            <a href="<?=$room->url?>" class="simple-btn"><span><?=Yii::t('translate', 'more')?></span></a>
                        </div>
                        <div class="room-description">
                            <div class="facilities-block">
                                <h6><?=Yii::t('translate', 'Available amenities')?>:</h6>
                                <ul class="facilities-list">
                                    <?php foreach($room->features_preview as $index => $reviewItem) {
                                        $d = 'right';
                                        if ($index % 2 == 0) {
                                            $d = 'left';
                                        }
                                        ?>
                                        <li>
                                            <div class="img-block"> <i class="fa <?=$reviewItem->icon?>"></i></div>
                                            <div class="desc <?=$d?>"><?=$reviewItem->lang_name?></div>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <a href="<?=$room->url?>" class="arrow-link"><span><?=Yii::t('translate', 'all')?></span> <i class="icon icon-long_arrow"></i></a>
                            </div>
                            <ul class="room-more-info">
                                <li>
                                    <div class="img-block"><i class="icon icon-key"></i></div>
                                    <span><?=Yii::t('translate', 'fr1')?></span>
                                </li>
                                <li>
                                    <div class="img-block"><i class="icon icon-card"></i></div>
                                    <span><?=Yii::t('translate', 'fr2')?></span>
                                </li>
                            </ul>
                            <a href="<?=$room->url?>" class="simple-btn"><span>Подробнее</span></a>
                        </div>
                        <div class="slider-container">
                            <div class="slider-big">
                                <div class="room-arrows slide-arrows">
                                    <div class="arrow arrow-prev"><i class="icon icon-slider_prev"></i></div>
                                    <div class="arrow arrow-next"><i class="icon icon-slider_next"></i></div>
                                </div>
                                <div class="room-slider room-single-slider">
                                    <?php if (!empty($room->image1)) {
                                        $filename = explode('.', basename($room->image1));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image2)) {
                                        $filename = explode('.', basename($room->image2));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image3)) {
                                        $filename = explode('.', basename($room->image3));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image4)) {
                                        $filename = explode('.', basename($room->image4));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image5)) {
                                        $filename = explode('.', basename($room->image5));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image6)) {
                                        $filename = explode('.', basename($room->image6));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image7)) {
                                        $filename = explode('.', basename($room->image7));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image8)) {
                                        $filename = explode('.', basename($room->image8));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image9)) {
                                        $filename = explode('.', basename($room->image9));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image10)) {
                                        $filename = explode('.', basename($room->image10));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image11)) {
                                        $filename = explode('.', basename($room->image11));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image12)) {
                                        $filename = explode('.', basename($room->image12));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image13)) {
                                        $filename = explode('.', basename($room->image13));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                    <?php if (!empty($room->image14)) {
                                        $filename = explode('.', basename($room->image14));
                                        $image = "/images/thumbs/$filename[0]-975-560.$filename[1]";
                                        ?>
                                        <div class="slide"><img src="<?=$image?>" alt="" /></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="container">
            <div class="all-rooms">
                <a class="big-arrow-link" href="<?=Url::to(['site/index', 'alias' => Textpage::findOne(1)->alias])?>"><span><?=Yii::t('translate', 'allRooms')?></span> <i class="icon icon-big_long_arrow"></i></a>
            </div>
        </div>
    </section>

    <!-- =================== ГАЛЕРЕЯ ======================= -->
    <section class="gallery-section">
        <div class="container">
            <div class="gallery-container">
                <div class="gallery-arrows slide-arrows">
                    <div class="arrow arrow-prev"><i class="icon icon-slider_prev"></i></div>
                    <div class="arrow arrow-next"><i class="icon icon-slider_next"></i></div>
                </div>
                <div class="gallery-slider">
                    <div class="slide">
                        <div class="gallery-flex">
                            <div class="gallery-block"><img src="/images/gallery_pic_1.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_2.jpg" alt="" /></div>
                            <div class="gallery-block width2"><img src="/images/gallery_pic_3.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_4.jpg" alt="" /></div>
                            <div class="gallery-block width2"><img src="/images/gallery_pic_5.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_6.jpg" alt="" /></div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="gallery-flex">
                            <div class="gallery-block"><img src="/images/gallery_pic_2.jpg" alt="" /></div>
                            <div class="gallery-block width2"><img src="/images/gallery_pic_5.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_1.jpg" alt="" /></div>
                            <div class="gallery-block width2"><img src="/images/gallery_pic_3.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_4.jpg" alt="" /></div>
                            <div class="gallery-block"><img src="/images/gallery_pic_6.jpg" alt="" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- =================== ОТЗЫВЫ ======================= -->
    <section class="reviews-section">
        <div class="container">
            <h2><?=Yii::t('translate', 'reviewsHeader')?></h2>
            <div class="row">
                <div class="col-lg-2 col-sm-1">
                    <div class="reviews-arrows slide-arrows">
                        <div class="arrow arrow-prev"><i class="icon icon-slider_prev"></i></div>
                        <div class="arrow arrow-next"><i class="icon icon-slider_next"></i></div>
                    </div>
                </div>
                <div class="col-lg-10 col-sm-11" style="position: relative;">
                    <div class="reviews-slider">
                        <?php foreach($reviewResult as $index => $reviews) { ?>
                            <div class="slide">
                                <?php foreach($reviews as $review) {
                                    echo $this->render('@frontend/views/review/_item', ['model' => $review]);
                                } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="total-rating">
                        <div class="value"><?=$averageMark?></div>
                        <p><strong><?=Yii::t('translate', 'mark')?></strong> <?=Yii::t('translate', 'po')?> <?=$reviewsCount?> <?=Yii::t('translate', 'reviews')?></p>
                    </div>
                    <div class="reviews-links">
                        <a class="btn popup-open" href="#review-popup" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'takeReview')?></span></a>
                        <a href="<?=Url::to(['site/index', 'alias' => Textpage::findOne(8)->alias])?>" class="big-arrow-link"><span><?=Yii::t('translate', 'allReviews')?></span> <i class="icon icon-big_long_arrow"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <?=$this->render('@frontend/views/blocks/contacts')?>

</div>
<!--//main-content-->