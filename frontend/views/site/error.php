<?php

use common\models\Mainpage;
use common\models\Textpage;
use yii\helpers\Url;

$this->params['seo_title'] = $model->lang_seo_title;
$this->params['seo_description'] = $model->lang_seo_description;
$this->params['seo_keywords'] = $model->lang_seo_keywords;
$this->params['name'] = $model->lang_name;

$mainPage = Mainpage::findOne(1);

?>

<!--main-content-->
<section class="order-title-section">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">

            </div>
        </div>
    </div>
    <div class="container">
        <ul class="breadcrumbs">
            <li><span><?php
                    if (Yii::$app->language == 'en') {
                        echo '404 – page not found';
                    } else {
                        echo '404 – страница не найдена';
                    }
                    ?></span></li>
        </ul>
        <h1><?php
            if (Yii::$app->language == 'en') {
                echo '<small>404 – </small>page not found';
            } else {
                echo '<small>404 – </small>страница не найдена';
            }
            ?></h1>
        <div class="year-price">
            <p><?php
                if (Yii::$app->language == 'en') {
                    echo 'The requested page has never been on our website or it has been removed.';
                } else {
                    echo 'Запрашиваемой страницы никогда не было на нашем сайте или она была удалена.';
                }
                ?></p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</section>
<!--//main-content-->