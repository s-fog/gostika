<?php

use common\models\Room;
use common\models\Textpage;
use yii\helpers\Url;

$elements = [0 => ['url' => '/']];

$i = 1;

foreach(Textpage::find()->all() as $textpage) {
    $elements[$i]['url'] = $textpage->url;
    $i++;
}

foreach(Room::find()->all() as $room) {
    $elements[$i]['url'] = $room->url;
    $i++;
}

$str = '<?xml version="1.0" encoding="utf-8"?><!--Generated by Screaming Frog SEO Spider 8.3-->
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

foreach($elements as $item) {
    $str .= "<url><loc>https://gostikahotel.ru$item[url]</loc></url>";
}

$str .= '</urlset>';

echo $str;

?>
