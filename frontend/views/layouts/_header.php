<?php

use common\models\Textpage;
use yii\helpers\Url;

$mainPage = \common\models\Mainpage::findOne(1);
$phone = $mainPage->phone;
$nomera = Textpage::findOne(1);
$language = Yii::$app->language;
?>
<!--header-->
<header class="main-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-10">
                <a href="/" class="main-logo"><img src="/images/main_logo.png" alt="" /></a>
            </div>
            <div class="col-md-8 col-xs-2">
                <div class="header-options">
                    <ul class="header-menu">
                        <?php foreach(Textpage::find()
                            ->where(['parent_id' => 0])
                            ->andWhere('id <> 8 AND id <> 9')
                            ->orderBy(['sortOrder' => SORT_ASC])
                            ->all() as $index => $textpage) { ?>
                            <?php if ($textpage->id == 1) { ?>
                                <li><a href="<?=$nomera->url?>"><?=$nomera->lang_name?></a><i class="icon icon-select_arrow"></i>
                                    <ul class="room-categories">
                                        <?php foreach(Textpage::findAll(['parent_id' => 1]) as $index => $model) {
                                            switch($index) {
                                                case 0: $icon = 'icon-standart';break;
                                                case 1: $icon = 'icon-halflux';break;
                                                case 2: $icon = 'icon-lux';break;
                                                default: $icon = '';
                                            }
                                            ?>
                                            <li>
                                                <a href="<?=$model->url?>" class="active-category">
                                                    <div class="img-block"><i class="icon <?=$icon?>"></i></div>
                                                    <h5><?=$model->lang_name?></h5>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                            <?php } else {
                                echo $textpage->liLink();
                            } ?>
                        <?php } ?>
                    </ul>
                    <a class="phone" href="tel:<?=str_replace([' ', '(', ')'], ['', '', ''], $phone)?>"><?=$phone?></a>
                    <a href="#recall-popup" class="simple-btn popup-open" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'callback')?></span></a>
                    <ul class="languages-list">
                        <li><a href="<?=Url::to(['site/index', 'language' => 'ru'])?>"<?=($language == 'ru') ? ' class="active"' : ''?>>ru</a></li>
                        <li><a href="<?=Url::to(['site/index', 'language' => 'en'])?>"<?=($language == 'en') ? ' class="active"' : ''?>>en</a></li>
                    </ul>
                </div>
                <div class="mobile-buter">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>
<!--//header-->