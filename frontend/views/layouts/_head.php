<?php

use yii\helpers\Html;

?>
    <meta charset="<?=Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?=(isset($this->params['seo_title']) && !empty($this->params['seo_title']))? $this->params['seo_title'] : $this->params['name'].' | Gostika'?></title>
    <?=(isset($this->params['seo_description']) && !empty($this->params['seo_description']))? '<meta name="description" content="'.$this->params['seo_description'].'">' : ''?>
    <?=(isset($this->params['seo_keywords']) && !empty($this->params['seo_keywords']))? '<meta name="keywords" content="'.$this->params['seo_keywords'].'">' : ''?>

    <meta name="og:title" content="<?=(isset($this->params['seo_title']) && !empty($this->params['seo_title']))? $this->params['seo_title'] : $this->params['name'].' | Gostika'?>">
    <?=(isset($this->params['seo_description']) && !empty($this->params['seo_description']))? '<meta name="og:description" content="'.$this->params['seo_description'].'">' : ''?>
    <meta name="og:image" content="/images/main_logo.png">
    <?php
        if (Yii::$app->language == 'en') {
            echo '<link rel="alternate" href="https://gostikahotel.ru/" hreflang="ru" />';
        } else {
            echo '<link rel="alternate" href="https://gostikahotel.ru/en" hreflang="en" />';
        }
    ?>
        <?php $this->head() ?>