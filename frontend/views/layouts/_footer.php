<?php

use common\models\Textpage;
use frontend\models\ReviewForm;
use frontend\models\SimpleForm;
use yii\widgets\ActiveForm;

$mainPage = \common\models\Mainpage::findOne(1);
$phone = $mainPage->phone;
?>
<!--footer-->
<footer>
    <div class="footer-top">
        <div class="container">
            <div class="footer-flex">
                <div class="footer-logo"><img src="/images/main_logo.png" alt="" /></div>
                <ul class="footer-menu">
                    <?php foreach(Textpage::find()
                        ->where(['parent_id' => 0])
                        ->andWhere('id <> 8 AND id <> 9')
                        ->orderBy(['sortOrder' => SORT_ASC])
                        ->all() as $index => $textpage) {
                        echo $textpage->liLink();
                    } ?>
                </ul>
                <a class="phone" href="tel:<?=str_replace([' ', '(', ')'], ['', '', ''], $phone)?>"><?=$phone?></a>
                <a href="#recall-popup" class="simple-btn popup-open" data-effect="mfp-zoom-in"><span><?=Yii::t('translate', 'callback')?></span></a>
            </div>
        </div>
    </div>
    <?php
    $conf = Textpage::findOne(9);
    ?>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-flex">
                <p><?=date('Y')?> <?=Yii::t('translate', 'f1')?></p>
                <a href="<?=$conf->url?>"><?=$conf->lang_name?></a>
            </div>
        </div>
    </div>
</footer>
<!--//footer-->

<!-- =================== МОДАЛКИ ======================= -->

<!--Перезвоните мне-->
<div id="recall-popup" class="popup-modal mfp-hide mfp-with-anim">
    <div class="popup-content">
        <div class="popup-flex">
            <div class="popup-img"><img src="/images/recall_pic.jpg" alt="" /></div>
            <div class="popup-information">
                <form action="#" method="post" class="simple-form sendForm">
                    <h4><?=Yii::t('translate', 'recall')?></h4>
                    <div class="title-desc"><?=Yii::t('translate', 'fill')?></div>
                    <div class="input-block">
                        <input type="text" placeholder="<?=Yii::t('translate', 'name')?>" name="name" required="required" />
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="<?=Yii::t('translate', 'phone')?>" class="phone-mask" name="phone" required="required" />
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="<?=Yii::t('translate', 'email')?>" name="email" />
                    </div>
                    <div class="input-block">
                        <textarea placeholder="<?=Yii::t('translate', 'comm')?>" name="comm"></textarea>
                    </div>
                    <button type="submit" class="btn big-btn"><span><?=Yii::t('translate', 'recall2')?></span></button>
                    <input type="hidden" name="type" value="Заказан обратный звонок на сайте Gostika">
                    <input type="text" name="BC" class="BC" value="">
                </form>
            </div>
        </div>
        <button class="mfp-close" type="button" title="Close (Esc)"><i class="icon icon-close_popup"></i></button>
    </div>
</div>

<!--//Перезвоните мне-->

<!--Забронировать номер-->
<div id="order-popup" class="popup-modal mfp-hide mfp-with-anim">
    <div class="popup-content">
        <div class="popup-flex">
            <div class="popup-img"><img src="/images/order_pic.jpg" alt="" /></div>
            <div class="popup-information">
                <form action="#" method="post" class="order-form simple-form order-validate">
                    <h4>Забронировать <small>номер</small></h4>
                    <div class="order-inputs">
                        <label class="input-label">
                            <div class="label-titile"><i class="icon icon-calendar"></i>Заезд</div>
                            <div class="order-date">25</div>
                            <div class="order-month">Апрель</div>
                            <i class="icon icon-select_arrow"></i>
                            <input type="text" class="datepicker calendar-input" />
                        </label>
                        <label class="input-label">
                            <div class="label-titile"><i class="icon icon-calendar"></i>Отьезд</div>
                            <div class="order-date">16</div>
                            <div class="order-month">Сентябрь</div>
                            <i class="icon icon-select_arrow"></i>
                            <input type="text" class="datepicker calendar-input" />
                        </label>
                        <label class="input-label">
                            <div class="label-titile"><i class="icon icon-man"></i>Человек</div>
                            <div class="order-date">2</div>
                            <div class="order-month">взрослых</div>
                            <i class="icon icon-select_arrow"></i>
                            <input type="text" class="datepicker calendar-input" />
                        </label>
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="Имя" name="name" />
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="Телефон" class="phone-mask" name="phone" />
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="Е-mail" name="email" />
                    </div>
                    <div class="input-block">
                        <textarea placeholder="Комментарий" name="textarea"></textarea>
                    </div>
                    <button type="submit" class="btn big-btn"><span>Забронировать</span></button>
                    <div class="order-phone">Возникли вопросы?  Тел.  <a href="tel://+7-809-345-3223">+7 (809) 345 32 23</a></div>
                </form>
                <!--<div class="thanks-message">
                    <h4>Спасибо!</h4>
                    <div class="title-desc"><strong>Мы получили Вашу заявку.</strong></div>
                    <div class="title-desc">В ближайшее время с Вами свяжется наш менеджер для уточнения деталей</div>
                </div>-->
            </div>
        </div>
        <button class="mfp-close" type="button" title="Close (Esc)"><i class="icon icon-close_popup"></i></button>
    </div>
</div>
<!--//Забронировать номер-->

<!--Оставьте отзыв-->
<div id="review-popup" class="popup-modal mfp-hide mfp-with-anim">
    <div class="popup-content">
        <div class="popup-flex">
            <div class="popup-img"><img src="/images/order_pic.jpg" alt="" /></div>
            <div class="popup-information">
                <form action="#" method="post" class="order-form simple-form sendForm">
                    <h4><?=Yii::t('translate', 'takeReview2')?></h4>
                    <div class="input-block">
                        <input type="text" placeholder="<?=Yii::t('translate', 'name')?>" name="name" required="required"/>
                    </div>
                    <div class="input-block">
                        <input type="text" placeholder="<?=Yii::t('translate', 'phone')?>" class="phone-mask" name="phone" required="required" />
                    </div>
                    <div id="reviewStars-input-wrapper" style="overflow: hidden;">
                        <div id="reviewStars-input">
                            <input id="star-4" type="radio" name="mark" value="1"/>
                            <label title="gorgeous" for="star-4"></label>

                            <input id="star-3" type="radio" name="mark" value="2"/>
                            <label title="good" for="star-3"></label>

                            <input id="star-2" type="radio" name="mark" value="3"/>
                            <label title="regular" for="star-2"></label>

                            <input id="star-1" type="radio" name="mark" value="4"/>
                            <label title="poor" for="star-1"></label>

                            <input id="star-0" type="radio" name="mark" value="5" checked/>
                            <label title="bad" for="star-0"></label>
                        </div>
                    </div>
                    <div class="input-block">
                        <textarea placeholder="<?=Yii::t('translate', 'text')?>" name="text" required="required"></textarea>
                    </div>
                    <button type="submit" class="btn big-btn"><span><?=Yii::t('translate', 'sendReview')?></span></button>
                    <input type="hidden" name="type" value="Оставлен отзыв на сайте Gostika">
                    <input type="hidden" name="review"value="1">
                    <input type="text" name="BC" class="BC" value="">
                </form>
            </div>
        </div>
        <button class="mfp-close" type="button" title="Close (Esc)"><i class="icon icon-close_popup"></i></button>
    </div>
</div>

<div id="success" class="popup-modal mfp-hide mfp-with-anim">
    <div class="popup-content">
        <div id="success__text">Сообщение успешно отправлено.<br>
            Менеджер перезвонит Вам в ближайшее время.</div>
        <button class="mfp-close" type="button" title="Close (Esc)"><i class="icon icon-close_popup"></i></button>
    </div>
</div>

<!--//Оставьте отзыв-->