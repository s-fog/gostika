<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CustomDate extends Model
{
    public static function month($time) {
        $month = date('m', $time);
        $result = '';

        switch($month) {
            case '01': {
                $result = 'января';
                break;
            }
            case '02': {
                $result = 'февраля';
                break;
            }
            case '03': {
                $result = 'марта';
                break;
            }
            case '04': {
                $result = 'апреля';
                break;
            }
            case '05': {
                $result = 'мая';
                break;
            }
            case '06': {
                $result = 'июня';
                break;
            }
            case '07': {
                $result = 'июля';
                break;
            }
            case '08': {
                $result = 'августа';
                break;
            }
            case '09': {
                $result = 'сентября';
                break;
            }
            case '10': {
                $result = 'октября';
                break;
            }
            case '11': {
                $result = 'ноября';
                break;
            }
            case '12': {
                $result = 'декабря';
                break;
            }
        }

        return $result;
    }

    public static function month2($time) {
        $month = date('m', $time);
        $result = '';

        switch($month) {
            case '01': {
                $result = 'Январь';
                break;
            }
            case '02': {
                $result = 'Февраль';
                break;
            }
            case '03': {
                $result = 'Март';
                break;
            }
            case '04': {
                $result = 'Апрель';
                break;
            }
            case '05': {
                $result = 'Май';
                break;
            }
            case '06': {
                $result = 'Июнь';
                break;
            }
            case '07': {
                $result = 'Июль';
                break;
            }
            case '08': {
                $result = 'Август';
                break;
            }
            case '09': {
                $result = 'Сентябрь';
                break;
            }
            case '10': {
                $result = 'Октябрь';
                break;
            }
            case '11': {
                $result = 'Ноябрь';
                break;
            }
            case '12': {
                $result = 'Декабрь';
                break;
            }
        }

        return $result;
    }

    public static function smallMonth($time) {
        $month = date('m', $time);
        $result = '';

        switch($month) {
            case '01': {
                $result = 'янв';
                break;
            }
            case '02': {
                $result = 'фев';
                break;
            }
            case '03': {
                $result = 'мар';
                break;
            }
            case '04': {
                $result = 'апр';
                break;
            }
            case '05': {
                $result = 'мая';
                break;
            }
            case '06': {
                $result = 'июн';
                break;
            }
            case '07': {
                $result = 'июл';
                break;
            }
            case '08': {
                $result = 'авг';
                break;
            }
            case '09': {
                $result = 'сен';
                break;
            }
            case '10': {
                $result = 'окт';
                break;
            }
            case '11': {
                $result = 'ноя';
                break;
            }
            case '12': {
                $result = 'дек';
                break;
            }
        }

        return $result;
    }

    public static function weekDay($dayNumber) {
        $result = '';

        switch($dayNumber) {
            case 1: {
                $result = 'Понедельник';
                break;
            }
            case 2: {
                $result = 'Вторник';
                break;
            }
            case 3: {
                $result = 'Среда';
                break;
            }
            case 4: {
                $result = 'Четверг';
                break;
            }
            case 5: {
                $result = 'Пятница';
                break;
            }
            case 6: {
                $result = 'Суббота';
                break;
            }
            case 7: {
                $result = 'Воскресенье';
                break;
            }
        }

        return $result;
    }

    public static function weekDaySmall($dayNumber) {
        $result = '';

        switch($dayNumber) {
            case 1: {
                $result = 'Пон';
                break;
            }
            case 2: {
                $result = 'Вт';
                break;
            }
            case 3: {
                $result = 'Ср';
                break;
            }
            case 4: {
                $result = 'Чет';
                break;
            }
            case 5: {
                $result = 'Пят';
                break;
            }
            case 6: {
                $result = 'Суб';
                break;
            }
            case 7: {
                $result = 'Вос';
                break;
            }
        }

        return $result;
    }
}
