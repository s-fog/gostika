<?php

namespace frontend\models;

use Yii;

class SimpleForm extends Forms
{
    public function rules()
    {
        return [
            [['phone', 'name'], 'required'],
        ];
    }
}