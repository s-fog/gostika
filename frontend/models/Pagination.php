<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

class Pagination
{
    public static function pagination($allProductCount, $page, $limit, $rooms = false) {
        if ($limit >= $allProductCount) {
            return [
                'html' => '',
                'prev' => '',
                'next' => ''
            ];
        } else {
            $pages = [];
            $_GET['page'] = (isset($_GET['page']))? $_GET['page'] : 1;
            $lastPage = ceil($allProductCount / $limit);
            $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
            $prev = (($_GET['page'] - 1) == 1)? $uri_parts[0] : $uri_parts[0] . '?page=' . ($_GET['page'] - 1);

            if ($page != 1) {
                //$pages[] = '<li><a href="' . $uri_parts[0] . '" class="pagination__item pagination__first">&lt;&lt;</a></li>';
                //$pages[] = '<li><a href="' . $prev . '" class="pagination__item pagination__first">&lt;</a></li>';
            }

            for($i = 1; $i <= $lastPage; $i++) {
                $link = ($i == 1)? $uri_parts[0] : $uri_parts[0] . '?page=' . $i;
                if ($i == $page) {
                    $pages[] = '<li><span class="pagination__item active-page">' . $i . '</span></li>';
                } else {
                    $pages[] = '<li><a href="' . $link . '" class="pagination__item">' . $i . '</a></li>';
                }
            }

            if ($page != $lastPage) {
                //$pages[] = '<li><a href="' . $uri_parts[0] . '?page=' . ($page + 1) . '" class="pagination__item pagination__last">&gt;</a></li>';
                //$pages[] = '<li><a href="' . $uri_parts[0] . '?page=' . ($lastPage) . '" class="pagination__item pagination__last">&gt;&gt;</a></li>';
            }

            $prev = '';
            $next = '';

            if ($page != 1) {
                $prev = 'https://'.$_SERVER['HTTP_HOST'].$uri_parts[0] . '?page='. ($page - 1);
            }

            if ($page != $lastPage) {
                $next = 'https://'.$_SERVER['HTTP_HOST'].$uri_parts[0] . '?page='. ($page + 1);
            }

            $allRooms = '';
            if ($rooms) {
                $allRooms = '<a href="" class="big-arrow-link"><span>'.Yii::t("translate", "allRooms").'</span> <i class="icon icon-big_long_arrow"></i></a>';
            }

            return [
                'html' => '<div class="all-rooms"><ul class="pagination">'.implode('', $pages).'</ul>'.$allRooms.'</div>',
                'prev' => $prev,
                'next' => $next
            ];
        }
    }
}