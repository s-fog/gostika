<?php

namespace frontend\models;

use common\models\Form;
use common\models\Review;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

class Forms extends Model
{
    public $name;
    public $phone;
    public $email;
    public $BC;
    public $type;
    public $comm;
    public $mark;
    public $text;

    public function send($post, $review = false) {

    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
        ];
    }
}
