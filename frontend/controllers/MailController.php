<?php

namespace frontend\controllers;

use common\models\Form;
use common\models\Mainpage;
use common\models\Review;
use frontend\models\DonorForm;
use frontend\models\QuestionForm;
use frontend\models\QuickForm;
use frontend\models\ReviewForm;
use frontend\models\SimpleForm;
use Yii;

class MailController extends \yii\web\Controller
{
    public function actionIndex() {
        if (!empty($_POST) && isset($_POST['type']) && !strlen($_POST['BC'])) {
            $labels = array(
                'name' => 'Имя',
                'email' => 'E-mail',
                'phone' => 'Телефон',
                'comm' => 'Комментарии',
                'mark' => 'Оценка',
                'text' => 'Текст',
                'reviewLink' => 'Ссылка на отзыв в админке',
            );
            $post = $_POST;

            $type = $post['type'];
            $msg = '';
            $to = Mainpage::findOne(1)->email;
            $headers = "Content-type: text/html; charset=\"utf-8\"\n";
            $headers .= "From: <$to>\n";
            $headers .= "MIME-Version: 1.0\n";

            if (isset($post['review']) && $post['review'] == 1) {
                $review = new Review();
                $review->name = $post['name'];
                $review->en_name = '&nbsp;';
                $review->phone = $post['phone'];
                $review->date = time();
                $review->mark = $post['mark'];
                $review->text = $post['text'];
                $review->en_text = '&nbsp;';
                $review->header = '&nbsp;';
                $review->en_header = '&nbsp;';
                $review->save(false);
                $url = $_SERVER['HTTP_HOST'].'/www/admin/index.php?r=review/update&id='.$review->id;
                $post['reviewLink'] = "<a href='$url'>Ссылка</a>";
                unset($post['review']);
            } else {
                $form = new Form();
                $form->name = $post['name'];
                $form->phone = $post['phone'];
                $form->email = $post['email'];
                $form->comm = $post['comm'];
                $form->date = date('c');
                $form->save(false);
            }
            unset($post['type']);

            foreach($post as $name=>$value){
                $label = array_key_exists($name, $labels) ? $labels[$name] : $name;
                $value = htmlspecialchars($value);
                if(strlen($value)) {
                    if ($name == 'url') {
                        $msg .= "<p><b>$label</b>: <a href='$value'>$value</a></p>";
                    } else {
                        $msg .= "<p><b>$label</b>: ".urldecode($value)."</p>";
                    }
                }
            }

            $body = $msg;


            $emailSendError = false;
            foreach(explode(',', $to) as $email) {
                if(!mail($email, $type, $body, $headers)) {
                    $emailSendError = true;
                }
            }

            if ($emailSendError) {
                return 'error';
            } else {
                return 'success';
            }
        }
    }
}
