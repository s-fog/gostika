<?php
namespace frontend\controllers;

use common\models\Mainpage;
use common\models\Review;
use common\models\Room;
use common\models\Textpage;
use frontend\models\Pagination;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($alias = '', $alias2 = '', $alias3 = '')
    {
        if (empty($alias) && empty($alias2) && empty($alias3)) {
            $model = Mainpage::findOne(1);
            $reviews = Review::findAll(['active' => 1]);
            $reviewResult = [];
            $averageMark = 0;
            $i = 0;

            foreach($reviews as $index => $review) {
                $reviewResult[$i][] = $review;

                if (($index+1) % 2 == 0) {
                    $i++;
                }

                if ($index == 0) {
                    $averageMark = $review->mark;
                } else {
                    $averageMark = ($averageMark + $review->mark) / 2;
                }
            }

            return $this->render('index', [
                'model' => $model,
                'reviewResult' => $reviewResult,
                'averageMark' => $averageMark,
                'reviewsCount' => count($reviews),
            ]);
        } else if (!empty($alias) && !empty($alias2) && !empty($alias3)) {
            $model = Room::find()->where(['alias' => $alias3])->one();

            if ($model) {
                return $this->render('@frontend/views/room/view', [
                    'model' => $model
                ]);
            } else {
                throw new NotFoundHttpException;
            }
        } else if (!empty($alias) && !empty($alias2)) {
            $model = Textpage::find()->where(['alias' => $alias2])->one();

            if ($model) {
                $page = (!empty($_GET['page']))? $_GET['page']: 1;
                $limit = 4;
                $offset = ($page == 1)? 0 : $page * $limit - $limit;

                $rooms = Room::find()
                    ->where(['category' => $model->id])
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy(['id' => SORT_DESC])
                    ->all();
                $allRooms = Room::find()
                    ->where(['category' => $model->id])
                    ->orderBy(['id' => SORT_DESC])
                    ->all();
                $pagination = Pagination::pagination(count($allRooms), $page, $limit, true);

                return $this->render('@frontend/views/room/index', [
                    'model' => $model,
                    'rooms' => $rooms,
                    'pagination' => $pagination,
                ]);
            }
        } else if (!empty($alias)) {
            $model = Textpage::find()->where(['alias' => $alias])->one();

            if ($model) {
                switch($model->id) {
                    case 1: {
                        $page = (!empty($_GET['page']))? $_GET['page']: 1;
                        $limit = 6;
                        $offset = ($page == 1)? 0 : $page * $limit - $limit;

                        $rooms = Room::find()
                            ->limit($limit)
                            ->offset($offset)
                            ->orderBy(['id' => SORT_DESC])
                            ->all();
                        $allRooms = Room::find()
                            ->orderBy(['id' => SORT_DESC])
                            ->all();
                        $pagination = Pagination::pagination(count($allRooms), $page, $limit, true);

                        return $this->render('@frontend/views/room/index', [
                            'model' => $model,
                            'rooms' => $rooms,
                            'pagination' => $pagination,
                        ]);
                    }
                    case 5: {
                        return $this->render('@frontend/views/textpage/udobstva', [
                            'model' => $model,
                        ]);
                    }
                    case 7: {
                        return $this->render('@frontend/views/textpage/contacts', [
                            'model' => $model,
                        ]);
                    }
                    case 8: {
                        $page = (!empty($_GET['page']))? $_GET['page']: 1;
                        $limit = 4;
                        $offset = ($page == 1)? 0 : $page * $limit - $limit;

                        $reviews = Review::find()
                            ->where(['active' => 1])
                            ->limit($limit)
                            ->offset($offset)
                            ->orderBy(['date' => SORT_DESC])
                            ->all();
                        $allReviews = Review::find()
                            ->where(['active' => 1])
                            ->orderBy(['date' => SORT_DESC])
                            ->all();
                        $pagination = Pagination::pagination(count($allReviews), $page, $limit);
                        $averageMark = 0;

                        foreach($allReviews as $index => $review) {
                            if ($index == 0) {
                                $averageMark = $review->mark;
                            } else {
                                $averageMark = ($averageMark + $review->mark) / 2;
                            }
                        }

                        return $this->render('@frontend/views/review/index', [
                            'model' => $model,
                            'reviews' => $reviews,
                            'pagination' => $pagination,
                            'averageMark' => $averageMark,
                            'reviewsCount' => count($allReviews),
                        ]);
                    }
                    default: {
                        return $this->render('@frontend/views/textpage/index', [
                            'model' => $model,
                        ]);
                    }
                }
            } else {
                throw new NotFoundHttpException;
            }
        } else {
            throw new NotFoundHttpException;
        }
    }
    public function actionSitemap()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');
        return $this->renderPartial('sitemap');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    /*public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }*/
}
