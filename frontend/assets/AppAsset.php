<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/stylename.css',
        'css/font-awesome.min.css',
        'css/grid.css',
        'css/slick.css',
        'css/jquery-ui.min.css',
        'css/magnific-popup.css',
        'css/main.css',
        'css/back.css',
    ];
    public $js = [
        'js/jquery.matchheight-min.js',
        'js/slick.min.js',
        'js/jquery-ui.min.js',
        'js/datepicker-ru.js',
        'js/jquery.magnific-popup.min.js',
        'js/jquery.inputmask.bundle.min.js',
        'js/jquery.validate.min.js',
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'js/myjs.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
