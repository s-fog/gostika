$(document).ready(function() {
	
	/*Убирание placeholder*/
	 $('input, textarea').focus(function(){
	   $(this).data('placeholder',$(this).attr('placeholder'))
	   $(this).attr('placeholder','');
	 });
	 $('input, textarea').blur(function(){
	   $(this).attr('placeholder',$(this).data('placeholder'));
	 });
	
	
	// Слайдер отзывов
	$('.reviews-slider').on('init reInit', function(event, slick){
        $(this).append('<div class="slider-count"><p><span class="current">1</span> / <span class="total">'+slick.slideCount+'</span></p></div>');
    });
	$('.reviews-slider').slick({
	  dots: false,
	  fade: true,
	  speed: 400,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  adaptiveHeight: true,
	  prevArrow: $('.reviews-arrows').find('.arrow-prev'),
      nextArrow: $('.reviews-arrows').find('.arrow-next'),
	});
	$('.reviews-slider')
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
            // finally let's do this after changing slides
            $(this).find('.slider-count .current').html(currentSlide+1);
        });
        
      
	// Слайдер комнаты простой
	$('.room-single-slider').on('init reInit', function(event, slick){
        $(this).append('<div class="slider-count"><p><span class="current">1</span> / <span class="total">'+slick.slideCount+'</span></p></div>');
    });
    $('.room-single-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      fade: true,
    });
    $('.room-single-slider')
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
            // finally let's do this after changing slides
            $(this).find('.slider-count .current').html(currentSlide+1);
        });
	
	$('.room-arrows .arrow-prev').click(function(){
	     $(this).parents(".slider-big").find(".room-slider").slick('slickPrev');
	});
	$('.room-arrows .arrow-next').click(function(){
	     $(this).parents(".slider-big").find(".room-slider").slick('slickNext');
	});
	
	
	// Слайдер комнаты с навигацией
	$('.room-nav-slider').on('init reInit', function(event, slick){
        $(this).append('<div class="slider-count"><p><span class="current">1</span> / <span class="total">'+slick.slideCount+'</span></p></div>');
    });
    $('.room-nav-slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: false,
      fade: true,
      asNavFor: '.room-slider-small'
    });
    $('.room-nav-slider')
        .on('afterChange', function(event, slick, currentSlide, nextSlide){
            // finally let's do this after changing slides
            $(this).find('.slider-count .current').html(currentSlide+1);
        });
	
	// Слайдер комнаты маленький
	$('.room-slider-small').slick({
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  speed: 500,
	  dots: false,
	  arrows: false,
	  focusOnSelect: true,
	  asNavFor: '.room-nav-slider',
	  responsive: [
	    {
	      breakpoint: 1550,
	      settings: {
	        slidesToShow: 3,
	      }
	    },
	    {
	      breakpoint: 1170,
	      settings: {
	        slidesToShow: 2,
	      }
	    }
	    ,
	    {
	      breakpoint: 1019,
	      settings: {
	        slidesToShow: 3,
	      }
	    }
	  ]
	});
	
	
	
	/*слайдер галереи*/
	$('.gallery-slider').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  fade: true,
	  speed: 500,
	  dots: false,
	  arrows: true,
	  prevArrow: $('.gallery-arrows').find('.arrow-prev'),
      nextArrow: $('.gallery-arrows').find('.arrow-next'),
	});
	
	/*Календарь*/
	$( ".datepicker" ).datepicker({}, $.datepicker.regional[ "ru" ]);
	
	$(document).on("click", function(e) {
    	var elem = $(e.target);
	    if(!elem.hasClass("hasDatepicker") && 
	        !elem.hasClass("ui-datepicker") && 
	        !elem.hasClass("ui-icon") && 
	        !elem.hasClass("ui-datepicker-next") && 
	        !elem.hasClass("ui-datepicker-prev") && 
	        !$(elem).parents(".ui-datepicker").length){
	            $('.hasDatepicker').datepicker('hide');
	    }
	});
	
	$('.calendar-input').bind('focus', function(){ 
		$(this).parents(".input-label").addClass('active-label'); 
	});
	$('.calendar-input').bind('blur', function(){ 
		$(this).parents(".input-label").removeClass("active-label"); 
	});
	
	/*модалка*/
	$('.popup-open').magnificPopup({
	  removalDelay: 300, 
	  fixedContentPos: true,
	  callbacks: {
	    beforeOpen: function() {
	       this.st.mainClass = this.st.el.attr('data-effect');
	    }
	  },
	  midClick: true 
	});
	
	/*Валидация*/
	$('.call-validate').validate({
	  	rules: {
			name: "required",
			phone: "required"
		},
		messages: {
			name: "Введите имя",
			phone: "Введите телефон"
		}
	});
	
	$('.order-validate').validate({
	  	rules: {
			name: "required",
			phone: "required",
			email: "required"
		},
		messages: {
			name: "Введите имя",
			phone: "Введите телефон",
			email: "Введите Е-mail"
		}
	});
	
	$('.review-validate').validate({
	  	rules: {
			name: "required",
			phone: "required",
			email: "required",
			textarea: "required"
		},
		messages: {
			name: "Введите имя",
			phone: "Введите телефон",
			email: "Введите Е-mail",
			textarea: "Введите отзыв"
		}
	});
	
	/*Работа мобильного меню*/
	$(".mobile-buter").click(function(){
		$(".header-options").slideToggle(300);
		$(this).toggleClass("open-buter");
	});
	
	/*Фиксация линии*/
	$(window).scroll(function(){
		if ($(this).scrollTop() > 200) {
			$('#fixed-bottom').fadeIn(400);
		} else {
			$('#fixed-bottom').fadeOut(400);
		}
		if ($(this).scrollTop() > 60){
			$('header').addClass("fixedtop");	
		}
		else {
			$('header').removeClass("fixedtop");	
		}
	});
	
	var target = $('footer');
	var targetPos = target.offset().top;
	var winHeight = $(window).height();
	var scrollToElem = targetPos - winHeight;
	$(window).scroll(function(){
	  var winScrollTop = $(this).scrollTop();
	  if(winScrollTop > scrollToElem){
	    $('#fixed-bottom').addClass('stop');
	  }
	  else{
	  	$('#fixed-bottom').removeClass('stop');
	  }
	});
	
	$('.info-table .info-text').matchHeight();

	$('.sendForm').on('submit', function (e) {
		var form = $(this);

		$.post('/mail/index/', form.serialize(), function(response) {
			console.log(response);

			if (response == 'success') {
				$.magnificPopup.instance.close();

				setTimeout(function() {
					$.magnificPopup.open({
						items: {
							src: $('#success'),
							type: 'inline'
						}
					});

					setTimeout(function() {
						$.magnificPopup.instance.close();
					}, 2500);
				}, 500);

				form.trigger('reset');
			} else {
				alert('Ошибка');
			}
		});
		return false;
	});

	$('.switch-map').click(function() {
		var index = $(this).index();
		console.log(index);

		$('.maps__item').removeClass('active');
		$('.maps__item').eq(index + 1).addClass('active');
	});
	
});

$(window).load(function(){
	 $(".phone-mask").inputmask({
    	"mask": "+7 (999) 999-99-99",
    	"showMaskOnHover": false
    });    
    
});	

/*Карта*/
if($("#map").length > 0){
	ymaps.ready(function () {
	    var myMap = new ymaps.Map('map', {
	            center: [55.7471,37.7746],
	            zoom: 17,
	            scroll: false,
	            controls: ['zoomControl']
	        }, {
	            searchControlProvider: 'yandex#search'
	        }),

	        // Создаём макет содержимого.
	        MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
	            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
	        ),

	        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
	            hintContent: 'Перовская улица, 31'
	        }, {
	            // Опции.
	            // Необходимо указать данный тип макета.
	            iconLayout: 'default#image',
	            // Своё изображение иконки метки.
	            iconImageHref: 'images/map_marker.png',
	            // Размеры метки.
	            iconImageSize: [119, 89],
	            // Смещение левого верхнего угла иконки относительно
	            // её "ножки" (точки привязки).
	            iconImageOffset: [-45, -88]
	        });

	    myMap.geoObjects.add(myPlacemark);
	    myMap.behaviors.disable('scrollZoom');
	});
}
