<?php

namespace backend\models;

use Yii;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * This is the model class for table "order".
 */
class UploadFileDynamicForm extends \yii\base\Model
{
    public static function upload($model, $index, $attribute, $attributeFile, $thumbs = array(), $watermark = false) {
        /*echo '<pre>',print_r($_POST),'</pre>';
        echo '<pre>',print_r($_FILES),'</pre>';die();*/
        $file = UploadedFile::getInstance($model, $attributeFile);
        $result = '';

        $modelClassName = str_replace('\\', '/', $model->className());
        preg_match("#[a-zA-z]+/[a-zA-z]+/([a-zA-z]+)$#siU", $modelClassName, $match);
        $className = $match[1];

        //var_dump($file);die();
        if ($file) {
            $flag = true;
            $name = md5($file->baseName.time());
            $extention = $file->extension;

            if ($_FILES[$className]['type'][$attributeFile] == 'image/jpeg' || $_FILES[$className]['type'][$attributeFile] == 'image/jpg') {
                $filename = '/uploads/'.$name.time().'.'.$extention;
                $image = imagecreatefromjpeg($_FILES[$className]['tmp_name'][$attributeFile]);
                $flag = imagejpeg ($image, Yii::getAlias('@www').$filename, 75);
                imagedestroy($image);
            } else if ($_FILES[$className]['type'][$attributeFile] == 'image/png') {
                $filename = '/uploads/'.$name.time().'.'.$extention;
                $image = imagecreatefrompng($_FILES[$className]['tmp_name'][$attributeFile]);
                imagealphablending($image, false);
                imagesavealpha($image, true);
                $flag = imagepng($image, Yii::getAlias('@www').$filename, 9);
                imagedestroy($image);
            } else {
                $filename = '/uploads/'.$name.'.'.$extention;
                $flag = $file->saveAs(Yii::getAlias('@www').$filename);
            }


            if ($flag) {
                $result = $filename;

                foreach($thumbs as $thumb) {
                    UploadFile::doThumb($filename, $thumb, $watermark);
                }
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }


        if (!empty($_FILES[$className]['name'][$index][preg_replace('#^\[[0-9]+\](.*)$#siU', '$1', $attributeFile)])) {
            if ($result) {
                return $result;
            }
        } else {
            return $_POST[$className][$index][preg_replace('#^\[[0-9]+\](.*)$#siU', '$1', $attribute)];
        }
    }

    public static function doThumb($image, $thumb, $watermark) {
        $filename = explode('.', basename($image));
        $thumbCut = explode('x', $thumb);

        $thumbPath = '/images/thumbs/'.$filename[0].'-'.$thumbCut[0].'-'.$thumbCut[1].'.'.$filename[1];

        if (!file_exists(Yii::getAlias('@www') . $thumbPath)) {
            Image::thumbnail('@www' . $image, $thumbCut[0], $thumbCut[1])
                ->save(Yii::getAlias('@www'.$thumbPath), ['quality' => 80]);
        }
    }
}
