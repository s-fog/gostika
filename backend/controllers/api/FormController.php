<?php

namespace backend\controllers\api;

/**
* This is the class for REST controller "FormController".
*/

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

class FormController extends \yii\rest\ActiveController
{
public $modelClass = 'common\models\Form';
}
