<?php

namespace backend\controllers;

use backend\models\Model;
use backend\models\UploadFile;
use common\models\Feature;
use common\models\Room;
use Exception;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\base\Response;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
* This is the class for controller "RoomController".
*/
class RoomController extends \backend\controllers\base\RoomController
{
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Room::className(),
            ],
        ];
    }

    public function actionCreate()
    {
        $modelRoom = new Room;
        $modelsFeature = [new Feature];
        $modelRoom->scenario = 'create';

        if ($modelRoom->load($_POST)) {
            $modelRoom->image1 = UploadedFile::getInstance($modelRoom, "image1");

            $modelsFeature = Model::createMultiple(Feature::classname());
            Model::loadMultiple($modelsFeature, $_POST);

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFeature),
                    ActiveForm::validate($modelRoom)
                );
            }

            // validate all models
            $valid = $modelRoom->validate();
            $valid = Model::validateMultiple($modelsFeature) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();

                $modelRoom->image1 = UploadFile::upload($modelRoom, 'image1', 'image1', ['975x560', '520x400', '209x120']);
                if (!empty($_FILES['Room']['name']['image2'])) {
                    $modelRoom->image2 = UploadFile::upload($modelRoom, 'image2', 'image2', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image3'])) {
                    $modelRoom->image3 = UploadFile::upload($modelRoom, 'image3', 'image3', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image4'])) {
                    $modelRoom->image4 = UploadFile::upload($modelRoom, 'image4', 'image4', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image5'])) {
                    $modelRoom->image5 = UploadFile::upload($modelRoom, 'image5', 'image5', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image6'])) {
                    $modelRoom->image6 = UploadFile::upload($modelRoom, 'image6', 'image6', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image7'])) {
                    $modelRoom->image7 = UploadFile::upload($modelRoom, 'image7', 'image7', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image8'])) {
                    $modelRoom->image8 = UploadFile::upload($modelRoom, 'image8', 'image8', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image9'])) {
                    $modelRoom->image9 = UploadFile::upload($modelRoom, 'image9', 'image9', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image10'])) {
                    $modelRoom->image10 = UploadFile::upload($modelRoom, 'image10', 'image10', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image11'])) {
                    $modelRoom->image11 = UploadFile::upload($modelRoom, 'image11', 'image11', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image12'])) {
                    $modelRoom->image12 = UploadFile::upload($modelRoom, 'image12', 'image12', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image13'])) {
                    $modelRoom->image13 = UploadFile::upload($modelRoom, 'image13', 'image13', ['975x560', '520x400', '209x120']);
                }
                if (!empty($_FILES['Room']['name']['image14'])) {
                    $modelRoom->image14 = UploadFile::upload($modelRoom, 'image14', 'image14', ['975x560', '520x400', '209x120']);
                }

                try {
                    if ($flag = $modelRoom->save(false)) {
                        foreach ($modelsFeature as $modelFeature) {
                            $modelFeature->room_id = $modelRoom->id;
                            if (! ($flag = $modelFeature->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'model' => $modelRoom,
            'modelsFeature' => (empty($modelsFeature)) ? [new Feature] : $modelsFeature
        ]);
    }

    public function actionUpdate($id)
    {
        $modelRoom = $this->findModel($id);
        $modelsFeature = $modelRoom->features;

        $image1 = $modelRoom->image1;
        $image2 = $modelRoom->image2;
        $image3 = $modelRoom->image3;
        $image4 = $modelRoom->image4;
        $image5 = $modelRoom->image5;
        $image6 = $modelRoom->image6;
        $image7 = $modelRoom->image7;
        $image8 = $modelRoom->image8;
        $image9 = $modelRoom->image9;
        $image10 = $modelRoom->image10;
        $image11 = $modelRoom->image11;
        $image12 = $modelRoom->image12;
        $image13 = $modelRoom->image13;
        $image14 = $modelRoom->image14;

        if ($modelRoom->load($_POST)) {
            $oldIDs = ArrayHelper::map($modelsFeature, 'id', 'id');
            $modelsFeature = Model::createMultiple(Feature::classname(), $modelsFeature);
            Model::loadMultiple($modelsFeature, $_POST);
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFeature, 'id', 'id')));

            // ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFeature),
                    ActiveForm::validate($modelRoom)
                );
            }

            // validate all models
            $valid = $modelRoom->validate();
            $valid = Model::validateMultiple($modelsFeature) && $valid;

            if ($valid) {

                if (!empty($_FILES['Room']['name']['image1'])) {
                    $modelRoom->image1 = UploadFile::upload($modelRoom, 'image1', 'image1', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image1 = $image1;
                }
                if (!empty($_FILES['Room']['name']['image2'])) {
                    $modelRoom->image2 = UploadFile::upload($modelRoom, 'image2', 'image2', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image2 = $image2;
                }
                if (!empty($_FILES['Room']['name']['image3'])) {
                    $modelRoom->image3 = UploadFile::upload($modelRoom, 'image3', 'image3', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image3 = $image3;
                }
                if (!empty($_FILES['Room']['name']['image4'])) {
                    $modelRoom->image4 = UploadFile::upload($modelRoom, 'image4', 'image4', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image4 = $image4;
                }
                if (!empty($_FILES['Room']['name']['image5'])) {
                    $modelRoom->image5 = UploadFile::upload($modelRoom, 'image5', 'image5', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image5 = $image5;
                }
                if (!empty($_FILES['Room']['name']['image6'])) {
                    $modelRoom->image6 = UploadFile::upload($modelRoom, 'image6', 'image6', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image6 = $image6;
                }
                if (!empty($_FILES['Room']['name']['image7'])) {
                    $modelRoom->image7 = UploadFile::upload($modelRoom, 'image7', 'image7', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image7 = $image7;
                }
                if (!empty($_FILES['Room']['name']['image8'])) {
                    $modelRoom->image8 = UploadFile::upload($modelRoom, 'image8', 'image8', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image8 = $image8;
                }
                if (!empty($_FILES['Room']['name']['image9'])) {
                    $modelRoom->image9 = UploadFile::upload($modelRoom, 'image9', 'image9', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image9 = $image9;
                }
                if (!empty($_FILES['Room']['name']['image10'])) {
                    $modelRoom->image10 = UploadFile::upload($modelRoom, 'image10', 'image10', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image10 = $image10;
                }
                if (!empty($_FILES['Room']['name']['image11'])) {
                    $modelRoom->image11 = UploadFile::upload($modelRoom, 'image11', 'image11', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image11 = $image11;
                }
                if (!empty($_FILES['Room']['name']['image12'])) {
                    $modelRoom->image12 = UploadFile::upload($modelRoom, 'image12', 'image12', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image12 = $image12;
                }
                if (!empty($_FILES['Room']['name']['image13'])) {
                    $modelRoom->image13 = UploadFile::upload($modelRoom, 'image13', 'image13', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image13 = $image13;
                }
                if (!empty($_FILES['Room']['name']['image14'])) {
                    $modelRoom->image14 = UploadFile::upload($modelRoom, 'image14', 'image14', ['975x560', '520x400', '209x120']);
                } else {
                    $modelRoom->image14 = $image14;
                }

                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $modelRoom->save(false)) {
                        if (! empty($deletedIDs)) {
                            Feature::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($modelsFeature as $modelFeature) {
                            $modelFeature->room_id = $modelRoom->id;
                            if (! ($flag = $modelFeature->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'model' => $modelRoom,
            'modelsFeature' => (empty($modelsFeature)) ? [new Feature] : $modelsFeature
        ]);
    }

    public function actionRemoveimg() {
        $model = $this->findModel($_POST['modelId']);
        $model->$_POST['field'] = '';

        if ($model->save(false)) {
            return 'success';
        } else {
            return 'error';
        }
    }
}
