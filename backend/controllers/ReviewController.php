<?php

namespace backend\controllers;

use common\models\Review;

/**
* This is the class for controller "ReviewController".
*/
class ReviewController extends \backend\controllers\base\ReviewController
{
    public function actionCreate()
    {
        $model = new Review;

        if ($_POST) {
            $_POST['Review']['date'] = strtotime($_POST['Review']['date']);
        }

        try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(['index']);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Review model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($_POST) {
            $_POST['Review']['date'] = strtotime($_POST['Review']['date']);
        }

        if ($model->load($_POST) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $model->date = date('d.m.Y', $model->date);

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
