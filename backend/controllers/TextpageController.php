<?php

namespace backend\controllers;

use backend\models\UploadFile;
use common\models\Textpage;
use himiklab\sortablegrid\SortableGridAction;

/**
* This is the class for controller "TextpageController".
*/
class TextpageController extends \backend\controllers\base\TextpageController
{
    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Textpage::className(),
            ],
        ];
    }

    public function actionCreate()
    {
        $model = new Textpage;

        try {
            if ($model->load($_POST)) {
                $model->image = UploadFile::upload($model, 'image', 'image', []);

                if ($model->save()) {
                    return $this->redirect(['index']);
                }
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = $model->image;

        if ($model->load($_POST)) {
            if (!empty($_FILES['Textpage']['name']['image'])) {
                $model->image = UploadFile::upload($model, 'image', 'image', []);
            } else {
                $model->image = $image;
            }

            if ($model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
}
