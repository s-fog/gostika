<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Главная страница', 'url' => ['/site/index']],
                    ['label' => 'Страницы', 'url' => ['/textpage/index']],
                    ['label' => 'Номера', 'url' => ['/room/index']],
                    ['label' => 'Отзывы', 'url' => ['/review/index']],
                    ['label' => 'Формы', 'url' => ['/form/index']],
                ],
            ]
        ) ?>

    </section>

</aside>
