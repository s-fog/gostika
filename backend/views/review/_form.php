<?php

use kartik\checkbox\CheckboxX;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Review $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="review-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Review',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-danger',
    'fieldConfig' => [
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
    'label' => 'col-sm-2',
    #'offset' => 'col-sm-offset-4',
    'wrapper' => 'col-sm-8',
    'error' => '',
    'hint' => '',
    ],
    ],
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>

            <?= $form->field($model, 'active')->widget(CheckboxX::classname(), [
                'pluginOptions' => [
                    'threeState'=>false
                ]
            ])?>
<!-- attribute name -->
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<!-- attribute en_name -->
			<?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

<!-- attribute mark -->
			<?= $form->field($model, 'mark')->textInput() ?>

<!-- attribute header -->
			<?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

<!-- attribute en_header -->
			<?= $form->field($model, 'en_header')->textInput(['maxlength' => true]) ?>

<!-- attribute text -->
			<?= $form->field($model, 'text')->textarea() ?>

<!-- attribute en_text -->
			<?= $form->field($model, 'en_text')->textarea() ?>

<!-- attribute date -->
			<?= $form->field($model, 'phone')->textInput() ?>

            <?=$form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'format' => 'dd.mm.yyyy',
                    'autoclose'=>true,
                    'todayHighlight' => false
                ]
            ]);?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    Tabs::widget(
                 [
                    'encodeLabels' => false,
                    'items' => [ 
                        [
    'label'   => Yii::t('models', 'Review'),
    'content' => $this->blocks['main'],
    'active'  => true,
],
                    ]
                 ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

