<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\ReviewSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="review-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'en_name') ?>

		<?= $form->field($model, 'date') ?>

		<?= $form->field($model, 'mark') ?>

		<?php // echo $form->field($model, 'header') ?>

		<?php // echo $form->field($model, 'en_header') ?>

		<?php // echo $form->field($model, 'text') ?>

		<?php // echo $form->field($model, 'en_text') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
