<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Review $model
*/

$this->title = Yii::t('models', 'Review');
?>
<div class="giiant-crud review-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
