<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Form $model
*/

$this->title = Yii::t('models', 'Form') . ' Редактирование';
?>
<div class="giiant-crud form-update">

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
