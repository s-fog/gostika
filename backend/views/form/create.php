<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Form $model
*/

$this->title = Yii::t('models', 'Form');
?>
<div class="giiant-crud form-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
