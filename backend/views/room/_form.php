<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Room $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="room-form">

	<?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

	<?=Tabs::widget([
		'items' => [
			[
				'label'     =>  'Основное',
				'content'   =>  $this->render('_main', ['form' => $form, 'model' => $model]),
				'active'    =>  true
			],
			[
				'label'     =>  'Изображения',
				'content'   =>  $this->render('_images', [
					'form' => $form, 
					'model' => $model,
				])
			],
			[
				'label'     =>  'Характеристики',
				'content'   =>  $this->render('_features', [
					'form' => $form,
					'model' => $model,
					'modelsFeature' => $modelsFeature,
				])
			],
			[
				'label'     => 'SEO',
				'content'   =>  $this->render('_seo', ['form' => $form, 'model' => $model])
			]
		]
	]);?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

