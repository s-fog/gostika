<?php
use common\models\Textpage;
use kartik\checkbox\CheckboxX;
use yii\helpers\ArrayHelper;

?>
    <br>
<!-- attribute name -->
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

<!-- attribute en_name -->
<?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

<!-- attribute alias -->
<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<!-- attribute alias -->
<?= $form->field($model, 'category')->dropDownList([
    'Выберите категорию' => ArrayHelper::map(Textpage::findAll(['parent_id' => 1]), 'id', 'name')
]) ?>

<!-- attribute price -->
<?= $form->field($model, 'price')->textInput() ?>

<!-- attribute descr -->
<?= $form->field($model, 'descr')->textarea(['rows' => 6]) ?>

<!-- attribute en_descr -->
<?= $form->field($model, 'en_descr')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'show_on_main')->widget(CheckboxX::classname(), [
    'pluginOptions' => [
        'threeState'=>false
    ]
])?>

<?= $form->field($model, 'hot')->widget(CheckboxX::classname(), [
    'pluginOptions' => [
        'threeState'=>false
    ]
])?>

<?= $form->field($model, 'sale')->widget(CheckboxX::classname(), [
    'pluginOptions' => [
        'threeState'=>false
    ]
])?>
