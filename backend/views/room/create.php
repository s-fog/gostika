<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Room $model
*/

$this->title = Yii::t('models', 'Room');
?>
<div class="giiant-crud room-create">

    <?= $this->render('_form', [
    'model' => $model,
        'modelsFeature' => $modelsFeature,
    ]); ?>

</div>
