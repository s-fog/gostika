<?php

use kartik\widgets\FileInput;
use yii\helpers\Html;

?>
<br>

<?php
$script = <<< JS
    $('.img-delete').click(function() {
        var element = $(this);
        var field = $(this).data('field');
        var data = '_csrf-backend='+$('[name="csrf-token"]').val()+'&field='+field+'&modelId='+$(this).data('id');
        
        $.post('/admin/index.php?r=room/removeimg', data, function(response) {
            $('.'+field+'-img').remove();
            element.remove();
        });
        return false;
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>

<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image1,
    'name' => 'image1'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image2,
    'name' => 'image2'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image3,
    'name' => 'image3'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image4,
    'name' => 'image4'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image5,
    'name' => 'image5'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image6,
    'name' => 'image6'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image7,
    'name' => 'image7'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image8,
    'name' => 'image8'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image9,
    'name' => 'image9'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image10,
    'name' => 'image10'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image11,
    'name' => 'image11'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image12,
    'name' => 'image12'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image13,
    'name' => 'image13'
])?>
<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image14,
    'name' => 'image14'
])?>