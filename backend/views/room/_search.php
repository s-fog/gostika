<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View $this
* @var common\models\RoomSearch $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="room-search">

    <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    ]); ?>

    		<?= $form->field($model, 'id') ?>

		<?= $form->field($model, 'name') ?>

		<?= $form->field($model, 'en_name') ?>

		<?= $form->field($model, 'alias') ?>

		<?= $form->field($model, 'seo_title') ?>

		<?php // echo $form->field($model, 'en_seo_title') ?>

		<?php // echo $form->field($model, 'seo_keywords') ?>

		<?php // echo $form->field($model, 'en_seo_keywords') ?>

		<?php // echo $form->field($model, 'seo_h1') ?>

		<?php // echo $form->field($model, 'en_seo_h1') ?>

		<?php // echo $form->field($model, 'seo_description') ?>

		<?php // echo $form->field($model, 'en_seo_description') ?>

		<?php // echo $form->field($model, 'created_at') ?>

		<?php // echo $form->field($model, 'updated_at') ?>

		<?php // echo $form->field($model, 'price') ?>

		<?php // echo $form->field($model, 'descr') ?>

		<?php // echo $form->field($model, 'en_descr') ?>

		<?php // echo $form->field($model, 'hot') ?>

		<?php // echo $form->field($model, 'sale') ?>

		<?php // echo $form->field($model, 'coffe') ?>

		<?php // echo $form->field($model, 'light') ?>

		<?php // echo $form->field($model, 'bed') ?>

		<?php // echo $form->field($model, 'dinner') ?>

		<?php // echo $form->field($model, 'image1') ?>

		<?php // echo $form->field($model, 'image2') ?>

		<?php // echo $form->field($model, 'image3') ?>

		<?php // echo $form->field($model, 'image4') ?>

		<?php // echo $form->field($model, 'image5') ?>

		<?php // echo $form->field($model, 'image6') ?>

		<?php // echo $form->field($model, 'image7') ?>

		<?php // echo $form->field($model, 'image8') ?>

		<?php // echo $form->field($model, 'image9') ?>

		<?php // echo $form->field($model, 'image10') ?>

		<?php // echo $form->field($model, 'image11') ?>

		<?php // echo $form->field($model, 'image12') ?>

		<?php // echo $form->field($model, 'image13') ?>

		<?php // echo $form->field($model, 'image14') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
