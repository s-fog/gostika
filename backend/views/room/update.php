<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var common\models\Room $model
*/

$this->title = Yii::t('models', 'Room') . ' Редактирование';
?>
<div class="giiant-crud room-update">

    <?php echo $this->render('_form', [
    'model' => $model,
        'modelsFeature' => $modelsFeature,
    ]); ?>

</div>
