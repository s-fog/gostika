<?php
use kartik\widgets\FileInput;
use yii\helpers\Html;

if (!isset($showDelete)) {
    $showDelete = true;
}

echo '<div style="border: 1px solid grey;padding: 10px;">'.Html::img($image, ['width' => 250, 'class' => $name.'-img']);
if(!empty($image) && $name != 'image1' && $showDelete) {
?>
    <br>
    <br>
<button class="img-delete" data-field="<?=$name?>" data-id="<?=$model->id?>">Удалить</button>
    <br>
    <br>
<?php } echo $form->field($model, $name)->widget(FileInput::className(), [
    'pluginOptions' => [
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseLabel' =>  'Выберите изображение',
    ],
    'options' => ['accept' => 'image/*']
]); ?>
</div>
<br>