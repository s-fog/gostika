<?php
use common\models\Textpage;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;

?>
<br>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <!-- attribute en_name -->
<?= $form->field($model, 'en_name')->textInput(['maxlength' => true]) ?>

<?=$form->field($model, 'parent_id')->widget(Select2::classname(), [
    'data' => Textpage::getTextpages($model->id),
    'options' => ['placeholder' => 'Выберите родителя ...']
]);?>

<?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'html')->widget(CKEditor::className()) ?>

<?= $form->field($model, 'en_html')->widget(CKEditor::className()) ?>

<?= $form->field($model, 'anons')->widget(CKEditor::className()) ?>

<?= $form->field($model, 'en_anons')->widget(CKEditor::className()) ?>

<?php if ($model->id == 5) { ?>
    <?= $form->field($model, 'table')->textarea() ?>

    <?= $form->field($model, 'en_table')->textarea() ?>
<?php } ?>

<?=$this->render('@backend/views/blocks/image', [
    'form' => $form,
    'model' => $model,
    'image' => $model->image,
    'name' => 'image',
    'showDelete' => false
])?>