<?php

use common\models\Textpage;
use kartik\widgets\Select2;
use rmrevin\yii\fontawesome\component\Icon;
use rmrevin\yii\fontawesome\FontAwesome;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
* @var yii\web\View $this
* @var common\models\Textpage $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="textpage-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Textpage',
    ]
    );
    ?>

    <div class="">

        <?=Tabs::widget([
            'items' => [
                [
                    'label'     =>  'Основное',
                    'content'   =>  $this->render('_main', ['form' => $form, 'model' => $model]),
                    'active'    =>  true
                ],
                [
                    'label'     => 'SEO',
                    'content'   =>  $this->render('_seo', ['form' => $form, 'model' => $model])
                ]
            ]
        ]);?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? 'Создать' : 'Сохранить'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

